import React from "react";
import {Link} from 'react-router-dom'

const Header = () => {
  return (
    <div className="flex justify-between px-24 absolute w-full">
      <div className="flex items-center ">
        <Link to="/">
          <img src="/Logo.png" alt="" className="w-48" />
        </Link>
        <div className="flex pl-16">
          <h1 className="text-lg font-bold mr-14">Articles</h1>
          <h1 className="text-lg font-bold mr-14">Teacher</h1>
          <h1 className="text-lg font-bold mr-14">Subject</h1>
          <Link to="/share">
            <h1 className="text-lg font-bold">Mockups</h1>
          </Link>
        </div>
      </div>
      <div className="flex items-center">
          <div className="text-lg font-bold text-white px-16">Login</div>
          <div className="text-lg font-bold text-blue-default bg-blue-light text-white rounded-lg px-5 py-2">Sign up</div>
      </div>
    </div>
  );
};

export default Header;

import React from "react";

const Footer = () => {
  return (
    <div className="px-32 py-10 bg-gray-300">
      <div className="flex justify-between mb-10">
        <div>
          <img src="/Logo.png" alt="" className="w-40 -ml-5" />
          <div className="text-lg py-3">Website for study UX/UI Uxeducation</div>
        </div>
        <div>
          <div className="text-xl font-bold pt-5">Contact</div>
          <div className="text-lg pt-8">Inthraporn.ar@kmitl.ac.th</div>
          <div className="text-lg pt-5">+08x-xxx-xxxx</div>
        </div>
        <div>
          <div className="text-xl font-bold pt-5">About us</div>
          <div className="text-lg pt-8">Articles</div>
          <div className="text-lg pt-5">Teacher</div>
          <div className="text-lg pt-5">Subjects</div>
          <div className="text-lg pt-5">Mockups</div>
        </div>
        <div>
          <div className="text-xl font-bold pt-5">Newsletters</div>
          <div className="text-lg pt-8">Subscribe to get update and Information.</div>
          <div className="flex justfy-between mt-5 bg-white rounded-lg p-2 w-full">
              <input type="text" placeHolder="Enter your email" className="flex-grow" />
              <button className="p-3 bg-blue-default rounded-lg text-white">Subscribe</button>
          </div>
        </div>
      </div>
      <div>Copyright © 2021 UXEDUCATION. All rights reserved.</div>
    </div>
  );
};

export default Footer;

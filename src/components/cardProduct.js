import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faHeart } from '@fortawesome/free-regular-svg-icons'
import {Link} from 'react-router-dom'
import { WEB_API } from "../env"

const CardRedeem = ({ name, description, point, picture,id , image }) => {

    //   const WEBAPI = 'http://localhost:8888/api/'
    // const WEBAPI = 'http://122.155.0.74:7451/api/'

    return (
        <div >
            <img src={`${WEB_API}${image}`} className="h-44 sm:h-80 w-full bg-gray-300"></img>
            <div className="flex items-center justify-between mt-7">
                <p className="text-sm sm:text-2xl font-extrabold">{name}</p>
            </div>
            <div className="items-center mt-3">
                <div className="break-all" dangerouslySetInnerHTML={{ __html: description }}></div>
                <span className="text-lg sm:text-2xl text-black font-extrabold block mt-2">{point} บาท</span>
            </div>
            <div className="grid grid-cols-4 gap-x-6 mt-3">
                <Link to={`/etaxdetail/${id}`} className="col-start-1 col-end-5">
                    <div className="col-start-1 col-end-3">
                        <button className="w-full items-center bg-red-default text-xs sm:text-xl text-extrabold text-white  py-2 sm:py-3 hover:bg-red-light">ซื้อสินค้า</button>
                    </div>
                </Link>
            </div>
        </div>
    )
}

export default CardRedeem

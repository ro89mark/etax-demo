import React from "react"
import Product from "./page/product"
import EtaxDetail from "./page/etaxDetail"
import Notice from "./page/notice"
import { BrowserRouter as Router,Switch, Route } from "react-router-dom";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Product />
          </Route>
          <Route exact path="/etaxdetail/:id">
            <EtaxDetail />
          </Route>
          <Route exact path="/notice">
            <Notice/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

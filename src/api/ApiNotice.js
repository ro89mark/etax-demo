import axios from "axios";
import { Component } from "react";
import { WEB_API } from "../env"

class ApiNotice extends Component{
  
  static getInvocieByEmail = async (data) => {
      const result = await axios({
          url: `${WEB_API}getInvoiceByEmail`,
          method: "post",
          data: data
      });
      return result;
  };

  static getInvociePriceById = async (id) => {
      const result = await axios({
          url: `${WEB_API}getInvoicePriceById/${id}`,
          method: "post",
      });
      return result;
  };

  static insertNotice = async (data) => {
    const result = await axios({
        url: `${WEB_API}insertNotice`,
        method: "post",
        data: data
    });
    return result;
};

//   static getProductById = async (id) => {
//       const result = await axios({
//           url: `http://localhost:8888/api/ex_productbyId/${id}`,
//           method: "get",
//       });
//       return result;
//   };
}

export default ApiNotice;

import axios from "axios";
import { Component } from "react";
import { WEB_API } from "../env"

class ApiGetProduct extends Component {

    static getProduct = async () => {
        const result = await axios({
            url: `${WEB_API}ex_product`,
            method: "get",
        });
        return result;
    };

    static getProductById = async (id) => {
        const result = await axios({
            url: `${WEB_API}ex_productbyId/${id}`,
            method: "get",
        });
        return result;
    };
}

export default ApiGetProduct;

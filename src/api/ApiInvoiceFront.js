import axios from "axios";
import { Component } from "react";
import { WEB_API } from "../env"

class ApiInvoiceFront extends Component{
  
  static insertInvoiceFront = async (data) => {
      const result = await axios({
          url: `${WEB_API}InsertInvoiceFrontend`,
          method: "post",
          data: data
      });
      return result;
  };
}

export default ApiInvoiceFront;

// import AlertError from "../reusable/AlertError";
import Swal from 'sweetalert2'

export const CheckFile = ({ file, size, type = [], message = "" }) => {
  console.log("file.type::",file.type);
  if (!type.includes(file.type) && type.length > 0) {
    Swal.fire({
      icon: 'error',
      title: 'Oopss...',
      text: message ? message : `รองรับเฉพาะไฟล์ประเภท ${type.join(",")}`
    })
    // AlertError(
    //   null,
    //   message ? message : `รองรับเฉพาะไฟล์ประเภท ${type.join(",")}`
    // );
    return false;
  }
  if (file.size / 1000 / 1000 > size) {
    Swal.fire({
      icon: 'error',
      title: 'Oopss...',
      text: message ? message : `Please upload a file smaller than ${size} MB`
    })
    // AlertError(null, `Please upload a file smaller than ${size} MB`);
    return false;
  } else {
    return true;
  }
};

import React from 'react'
import Header from '../../components/Header'
import '../../style/font.css'
const mockupDetail = () => {
  return (
    <div>
      <Header />
      <div className="py-40 px-32">
        <div className="flex justify-between justify-center items-center">
          <div className="flex items-center">
            <img src="/images/share/user/user1.png" alt="" />
            <h5 className="text-xl font-semibold px-3">Jenny Wilson</h5>
          </div>
          <div className="">
            <button className="bg-gray-200 rounded-lg py-2 px-5">Like</button>
          </div>
        </div>
        <div className="flex flex-col items-center">
          <img src="/images/imgmockupDetail.png"  className="p-4"/>
          <img src="/images/imgmockupDetail2.png"  className="p-4"/>
          <span className="text-h3">A while ago I was able to do a little bit of work on the concept of a folio site for an interior designer & CG artist. I decided to share ideas that were not implemented. Different layouts for the home screen, photo interactions, minimal grid.</span>
        </div>
      </div>
    </div>
  )
}

export default mockupDetail

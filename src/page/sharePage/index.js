import React from "react";
import Header from "../../components/Header";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faEye } from "@fortawesome/free-solid-svg-icons";
import {Link} from 'react-router-dom'
const SharePage = () => {
  return (
    <div>
      <Header />
      <img src="/images/share/section1.png" alt="" className="w-full" />
      <div className="flex flex-col items-center px-20">
        <div className="flex py-20">
          <button className="text-xl bg-blue-default text-white rounded-lg px-5 py-2 mr-5">
            Human Interactive Design
          </button>
          <button className="text-xl border border-gray-400 text-gray-400 rounded-lg px-5 py-2">
            Design Interactive System
          </button>
        </div>
        <div className="grid grid-cols-3 gap-x-7 gap-y-10">
          <Link to="/mockupdetail">
          <div className="flex flex-col">
            <img src="/images/share/project/project1.png" alt="" />
            <div className="flex justify-between py-3">
              <div className="flex items-center">
                <img src="/images/share/user/user1.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Jenny Wilson</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center mr-3">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          </Link>
          <div className="flex flex-col">
            <img src="/images/share/project/project2.png" alt="" />
            <div className="flex justify-between py-3">
              <div className="flex items-center">
                <img src="/images/share/user/user2.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Jenny Wilson</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center mr-3">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project3.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user3.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Theresa Webb</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project4.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user4.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Annette Black</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project5.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user5.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Leslie Alexander</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project6.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user6.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Courtney Henry</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project7.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user7.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Esther Howard</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project8.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user8.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Darlene Robertson</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <img src="/images/share/project/project9.png" alt="" />
            <div className="flex justify-between py-3 px-2">
              <div className="flex items-center">
                <img src="/images/share/user/user9.png" alt="" />
                <h5 className="text-xl font-semibold px-3">Dianne Russell</h5>
              </div>
              <div className="flex items-center">
                <div className="flex items-center px-5">
                  <FontAwesomeIcon icon={faHeart} className="text-lg mr-2" />
                  <div>12</div>
                </div>
                <div className="flex items-center">
                  <FontAwesomeIcon icon={faEye} className="text-lg mr-2" />
                  <div>32</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-center pt-10 mb-36">
          <button className="py-2 px-3 bg-gray-300 rounded-lg">Load more Shorts</button>
        </div>
      </div>
    </div>
  );
};

export default SharePage;

import React from 'react'
import Header from '../../components/Header'
import '../../style/font.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar,faQuoteLeft } from "@fortawesome/free-solid-svg-icons";

const HomePage = () => {
  return (
    <div id="base">
      <Header />
      <div className="flex justify-between">
        <div className="flex flex-col justify-center pl-32">
          <span className="text-h1">Nonboring learning UX<br/> and Attractive UI</span>
          <span className="text-h2">Access to teachers and knowledge<br/> It's easy with <span className="text-h1-b">UXeDucation</span></span>
          <div className="">
            <button className="border-2 py-2 px-6 text-button bg-blue-default rounded-xl">UX/UI TEST</button>
          </div>
        </div>
        <div >
          <img src="/images/bg-1.png" alt="" className="w-full mb-16"/>
        </div>
      </div>
      <div>
        <img src="/images/bg-2.png" alt="" className="w-full"/>
      </div>
      <div className="flex flex-col justify-center items-center mt-16">
        <span className="text-h1">Teacher</span>
        <div className="w-full flex relative justify-center">
          <div className="rounded-lg bg-gray-200 bg-card mt-14 z-1">
          </div>
          <div className="size-card rounded-lg border-2 absolute z-2 bg-white">
            <img src="/images/img_teacher.png" alt="" className="w-full p-6"/>
            <div className="flex flex-col items-center">
              <span className="text-h2">ผศ.ดร.อินทราพร  อรัณยะนาค</span>
              <span className="text-h2 mt-4">Asst.Prof.Inthraporn Aranyanak, Ph.D. </span>
            </div>
          </div>
        </div>
        <hr className="line rounded-xl mt-10 bg-blue-default"/>
      </div>
      <div className="flex flex-col items-center bg-gray-200 mt-16">
        <span className="text-h1 py-10">What our students are saying</span>
        <div className="flex items-center">
          <div className="size-review bg-white">
            <div className="flex justify-between">
              <img src="/images/proflie.png"  className="p-4"/>
              <div className="flex flex-col justify-center">
                <span className="text-p">Anongnart Pongoatana</span>
                <span className="text-p1 mt-2">Student in HCI class</span>
              </div>
              <div className="flex justify-center items-center mb-6">
                <span className="text-p1">4.5</span>
                <FontAwesomeIcon icon={faStar} className="text-sm mx-2" />
              </div>
            </div>
            <hr/>
            <div>
              <span className="text-p1 mt-2 p-4">However rare side effects observed among children can be metabolic acidosis, coma, respiratory depression, and hypoglycemia-</span>
            </div>
            <div className="mt-10">
              <span className="text-p2 p-4">January 20, 2021</span>
            </div>
          </div>
          <div className="size-review2 mx-14">
            <div className="flex justify-between">
              <img src="/images/proflie.png"  className="p-4"/>
              <div className="flex flex-col justify-center">
                <span className="text-p">Anongnart Pongoatana</span>
                <span className="text-p1 mt-2">Student in HCI class</span>
              </div>
              <div className="flex justify-center items-center mb-6">
                <span className="text-p1">4.5</span>
                <FontAwesomeIcon icon={faStar} className="text-sm mx-2" />
              </div>
            </div>
            <hr/>
            <div>
              <div className="px-4 pt-4">
                <FontAwesomeIcon icon={faQuoteLeft} className="text-3xl mx-2" />
              </div>
              <span className="text-p1 mt-2 p-4">However rare side effects observed among children can be metabolic acidosis, coma, respiratory depression, and hypoglycemia-</span>
            </div>
            <div className="mt-6">
              <span className="text-p2 p-4">January 20, 2021</span>
            </div>
          </div>
          <div className="size-review bg-white">
            <div className="flex justify-between">
              <img src="/images/proflie.png"  className="p-4"/>
              <div className="flex flex-col justify-center">
                <span className="text-p">Anongnart Pongoatana</span>
                <span className="text-p1 mt-2">Student in HCI class</span>
              </div>
              <div className="flex justify-center items-center mb-6">
                <span className="text-p1">4.5</span>
                <FontAwesomeIcon icon={faStar} className="text-sm mx-2" />
              </div>
            </div>
            <hr/>
            <div>
              <span className="text-p1 mt-2 p-4">However rare side effects observed among children can be metabolic acidosis, coma, respiratory depression, and hypoglycemia-</span>
            </div>
            <div className="mt-10">
              <span className="text-p2 p-4">January 20, 2021</span>
            </div>
          </div>
        </div>
        <div className="flex items-center m-14">
          <div className="size-review bg-white mr-14">
            <div className="flex justify-between">
              <img src="/images/proflie.png"  className="p-4"/>
              <div className="flex flex-col justify-center">
                <span className="text-p">Anongnart Pongoatana</span>
                <span className="text-p1 mt-2">Student in HCI class</span>
              </div>
              <div className="flex justify-center items-center mb-6">
                <span className="text-p1">4.5</span>
                <FontAwesomeIcon icon={faStar} className="text-sm mx-2" />
              </div>
            </div>
            <hr/>
            <div>
              <span className="text-p1 mt-2 p-4">However rare side effects observed among children can be metabolic acidosis, coma, respiratory depression, and hypoglycemia-</span>
            </div>
            <div className="mt-10">
              <span className="text-p2 p-4">January 20, 2021</span>
            </div>
          </div>
          <div className="size-review bg-white">
            <div className="flex justify-between">
              <img src="/images/proflie.png"  className="p-4"/>
              <div className="flex flex-col justify-center">
                <span className="text-p">Anongnart Pongoatana</span>
                <span className="text-p1 mt-2">Student in HCI class</span>
              </div>
              <div className="flex justify-center items-center mb-6">
                <span className="text-p1">4.5</span>
                <FontAwesomeIcon icon={faStar} className="text-sm mx-2" />
              </div>
            </div>
            <hr/>
            <div>
              <span className="text-p1 mt-2 p-4">However rare side effects observed among children can be metabolic acidosis, coma, respiratory depression, and hypoglycemia-</span>
            </div>
            <div className="mt-10">
              <span className="text-p2 p-4">January 20, 2021</span>
            </div>
          </div>
        </div>
      </div>
      <div className="py-20 px-44">
        <div className="flex justify-between items-center">
          <span className="text-h1 py-10">Our Article and News</span>
          <button className="bg-blue-default rounded-lg py-2 px-5 text-white">Read more</button>
        </div>
        <div className="flex justify-between">
          <div>
            <img src="/images/imgacticle.png"  className="w-full"/>
          </div>
          <div className="flex flex-col ">
            <div className="flex">
              <div>
                <img src="/images/imgacticle1.png"  className="w-full"/>
              </div>
              <div className="flex flex-col px-4 py-2">
                <span className="text-p-none text-red-default">Technology</span>
                <span className="text-p my-2">Golang คืออะไร? ดียังไง?ทำไมคนใช้เยอะ?รวมสิ่งที่ควรรู้<br/>เกี่ยวกับ Golang</span>
                <span className="text-p1">หลายๆ คนที่สนใจงานด้าน Web Development หรือสนใจด้าน<br/>DevOps ก็คงเคยได้ยินเกี่ยวกับภาษา Golang มาบ้าง</span>
              </div>
            </div>
            <div className="flex mt-16">
              <div>
                <img src="/images/imgacticle1.png"  className="w-full"/>
              </div>
              <div className="flex flex-col px-4 py-2">
                <span className="text-p-none text-red-default">Technology</span>
                <span className="text-p my-2">Golang คืออะไร? ดียังไง?ทำไมคนใช้เยอะ?รวมสิ่งที่ควรรู้<br/>เกี่ยวกับ Golang</span>
                <span className="text-p1">หลายๆ คนที่สนใจงานด้าน Web Development หรือสนใจด้าน<br/>DevOps ก็คงเคยได้ยินเกี่ยวกับภาษา Golang มาบ้าง</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default HomePage

import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router'
import Header from '../../components/Header'
import '../../style/font.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faQuoteLeft } from "@fortawesome/free-solid-svg-icons";
import CardProduct from '../../components/cardProduct'
import ApiNotice from '../../api/ApiNotice';
import { CheckFile } from "../../utils/uploadfile";
import Swal from 'sweetalert2'

const Notice = () => {

  const history = useHistory()
  const [email, setEmail] = useState('')
  const [noticePrice, setNoticePrice] = useState('')
  const [invoiceId, setInvoiceId] = useState('')
  const [originBank, setOriginBank] = useState('')
  const [file, setFile] = useState('')

  const [invoice, setInvoice] = useState([])
  const [checkClickList, setCheckClickList] = useState(false)

  const func_attachFile = async (e) => {
    if (e.files.length > 0) {
      var file = e.files[0];

      var type = ["image/png", "image/jpg", "image/jpeg"];
      var message = "";
      var check = CheckFile({
        file,
        size: 1,
        type: type,
        message,
      });
      if (check) {
        setFile(file)
      }
    }
  };

  const getInvoice = () => {
    const data = {
      email: email
    }
    getInvoiceByEmail(data)
  }

  const getNoticePrice = async (e) => {
    if (e.target.value === "") {
      setNoticePrice('')
      return
    }
    const id = e.target.value
    setInvoiceId(id)
    try {
      const result = await ApiNotice.getInvociePriceById(id);
      const invoicePriceData = result.data.data
      if (result.status === 200) {
        setNoticePrice(invoicePriceData[0].subtotal)
      }
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Oopss...',
        text: error.response.data
      })
    }
  }

  const setDataToInsert = () => {
    if (invoiceId && noticePrice && originBank && file) {
      const formData = new FormData();
      formData.append("invoice_id", invoiceId);
      formData.append("price_front", noticePrice);
      formData.append("origin_bank", originBank);
      formData.append("file", file);
      insertNotice(formData)
      return
    }
    Swal.fire({
      icon: 'error',
      title: 'Oopss...',
      text: 'กรุณากรอกข้อมูลให้ครบถ้วน'
    })
  }

  const insertNotice = async (data) => {
    try {
      Swal.showLoading()
      const result = await ApiNotice.insertNotice(data);
      if (result.status === 200) {
        Swal.fire({
          icon: 'success',
          title: 'สำเร็จ',
          text: 'แจ้งโอนสำเร็จ'
        }).then(() => window.location = "/")
      }
    } catch (error) {
      console.log(error.response.data.message)
      if (error.response.data.message == "Cannot read property 'split' of undefined") {
        Swal.fire({
          icon: 'error',
          title: 'Oopss...',
          text: 'การแจ้งโอนมีปัญหากรุณาติดต่อเจ้าหน้าที่'
        }).then(() => window.location.reload())
      }
      else {
        Swal.fire({
          icon: 'error',
          title: 'Oopss...',
          text: error.response.data.data
        }).then(() => window.location.reload())
      }
    }
  }

  const getInvoiceByEmail = async (data) => {
    try {
      const result = await ApiNotice.getInvocieByEmail(data);
      const invoicedata = result.data.data
      if (result.status === 200) {
        setInvoice(invoicedata)
        Swal.fire({
          icon: 'success',
          title: 'สำเร็จ',
          text: 'ค้นหาใบกำกับภาษีสำเร็จ'
        })
        setCheckClickList(true)
      }
    } catch (error) {
      console.log(error.response)
      Swal.fire({
        icon: 'error',
        title: 'Oopss...',
        text: error.response.data
      })

    }
  }

  return (
    <div id="base" className="max-w-screen-2xl m-auto">
      <div className="max-w-screen-lg">
        <div className="my-8">
          <span className="text-6xl text-red-default font-extrabold">แจ้งชำระเงิน</span>
        </div>
        <div className="w-full px-3 mb-4">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
            อีเมล
          </label>
          <div className='flex'>
            <input onChange={(e) => { setEmail(e.target.value) }} className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text"></input>
            <button onClick={getInvoice} className="w-64 h-12 text-lg w-32 bg-red-default rounded text-white hover:bg-blue-700 ml-4 hover:bg-red-light">ค้นหาใบกำกับภาษี</button>
          </div>
          {checkClickList ? <div className='text-red-fireBrick'>*พบใบกำกับภาษีที่ยังไม่ได้ชำระเงิน {invoice.length} รายการ*</div> : null}
        </div>
        <div className="ml-3 mb-2">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
            เลือกใบกำกับภาษี
          </label>
          <select onChange={(e) => { getNoticePrice(e) }} className="w-1/2 border border-gray-300 text-gray-600 h-10 pl-2 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none">
            <option value="">กรุณาเลือกข้อมูล</option>
            {invoice.map((item) =>
              <option key={item.id} value={item.id}>
                {item.invoice_num}
              </option>
            )}
          </select>
        </div>
        <div className="ml-3 mb-2">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
            ธนาคารต้นทาง
          </label>
          <select onChange={(e) => { setOriginBank(e.target.value) }} className="w-1/2 border border-gray-300 text-gray-600 h-10 pl-2 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none">
            <option value="">กรุณาเลือกข้อมูล</option>
            <option>ธนาคารกสิกรไทย</option>
            <option>ธนาคารไทยพาณิชย์</option>
            <option>ธนาคารกรุงเทพ</option>
          </select>
        </div>
        <div className="w-full px-3">
          <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
            ยอดที่ต้องชำระ
          </label>
          <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" value={noticePrice} type="text" disabled></input>
        </div>
        <div className="p-3">
          <div className="mb-2"> <span>Attachments</span>
            <div className="relative h-40 rounded-lg border-dashed border-2 border-gray-200 bg-white flex justify-center items-center hover:cursor-pointer">
              <div className="absolute">
                <div className="flex flex-col items-center "> <i className="fa fa-cloud-upload fa-3x text-gray-200"></i> <span className="block text-gray-400 font-normal">Attach you files here</span> <span className="block text-gray-400 font-normal">or</span> <span className="block text-blue-400 font-normal">Browse files</span> <span className="block text-blue-400 font-normal text-red-fireBrick">*{file.name}*</span> </div>
              </div> <input onChange={(e) => func_attachFile(e.target)} type="file" name="myfile" class="h-full w-full opacity-0"></input>
            </div>
          </div>
          <div className="mt-3 text-center pb-3"> <button onClick={setDataToInsert} className="w-64 h-12 text-lg w-32 bg-red-default rounded text-white hover:bg-blue-700 mt-4 hover:bg-red-light">แจ้งชำระเงิน</button> </div>
        </div>
      </div>
    </div>
  )
}

export default Notice

import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams
} from "react-router-dom";
import Header from '../../components/Header'
import '../../style/font.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faQuoteLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import CardProduct from '../../components/cardProduct'
import { Link } from 'react-router-dom'
import ApiInvoiceFront from '../../api/ApiInvoiceFront';
import InputAddress from 'react-thailand-address-autocomplete'
import ApiGetProduct from '../../api/ApiGetProduct';
import Swal from 'sweetalert2'

const EtaxDetail = () => {

  const history = useHistory()
  const { id } = useParams()
  const [name, setName] = useState('')
  const [invoiceNum, setInvoiceNum] = useState('')
  const [address, setAddress] = useState('')
  const [province, setProvince] = useState('')
  const [district, setDistrict] = useState('')
  const [subDistrict, setSubDistrict] = useState('')
  const [postalCode, setPostalCode] = useState('')
  const [email, setEmail] = useState('')
  const [taxId, setTaxId] = useState('')
  const [total, setTotal] = useState('')
  const [subTotal, setSubTotal] = useState('')
  const [product, setProduct] = useState('')
  const [addressCustomer, setAddressCustomer] = useState({
    firstname: "",
    lastname: "",
    phonenumber: "",
    address: "",
    province: "",
    district: "",
    subdistrict: "",
    zipcode: "",
    is_default: 1,
    is_tax: 0
  })

  useEffect(() => {
    getDataProductById(id);
    return () => {
    }
  }, []);

  const getDataProductById = async (id) => {
    try {
      const result = await ApiGetProduct.getProductById(id);
      if (result.status === 200) {
        const { data } = result.data;
        setProduct(data[0]);
      }
    } catch (error) {
      console.log(error)
    }
  }

  const vat = 0.07 * product.price
  const priceAll = Number(vat) + Number(product.price)

  // const dataCard = [{ name: "obey", status: "sale" }, { name: "obey2", status: "sale" }, { name: "obey3", status: "sale" }, { name: "obey4", status: "sale" }]
  const styleAdrees = {
    display: "block",
    width: "512px",
    backgroundColor: "white",
    borderColor: "rgba(107, 114, 128)",
    borderWidth: "1px",
    borderRadius: "0.25rem",
    paddingTop: "0.75rem",
    paddingBottom: "0.75rem",
    paddingLeft: "1rem",
    paddingRight: "1rem",
    lineHeight: "1.25",
    height: "46px"
  }

  const onChange = (e) => {
    setAddressCustomer({
      ...addressCustomer,
      [e.target.name]: e.target.value
    })
  }
  const onSelect = (addresses) => {
    const { subdistrict, district, province, zipcode } = addresses
    setAddressCustomer({
      ...addressCustomer,
      province: province,
      district: district,
      subdistrict: subdistrict,
      zipcode: zipcode
    })
    console.log(addressCustomer)
  }

  const setDataInsert = (e) => {
    e.preventDefault();
    if (addressCustomer.province && addressCustomer.district && addressCustomer.subdistrict && addressCustomer.zipcode) {
      const data = {
        "name": name,
        "address": addressCustomer.address,
        "province": addressCustomer.province,
        "district": addressCustomer.district,
        "sub_district": addressCustomer.subdistrict,
        "postal_code": addressCustomer.zipcode,
        "email": email,
        "tax_id": taxId,
        "taxrate": vat.toFixed(2),
        "total": product.price,
        "subtotal": priceAll.toFixed(2),
        "product_name": product.name
      }
      insertInvoice(data)
      return
    }
    Swal.fire({
      icon: 'error',
      title: 'Oopss...',
      text: 'กรุณากรอกข้อมูลให้ครบถ้วน'
    })
    // const data = {
    //   "name": name,
    //   "address": addressCustomer.address,
    //   "province": addressCustomer.province,
    //   "district": addressCustomer.district,
    //   "sub_district": addressCustomer.subdistrict,
    //   "postal_code": addressCustomer.zipcode,
    //   "email": email,
    //   "tax_id": taxId,
    //   "taxrate": vat.toFixed(2),
    //   "total": product.price,
    //   "subtotal": priceAll.toFixed(2),
    //   "product_name": product.name
    // }
    // insertInvoice(data)
    // console.log(data)
  }

  const insertInvoice = async (data) => {

    try {
      const result = await ApiInvoiceFront.insertInvoiceFront(data);
      console.log(result)
      if (result.status === 200) {
        Swal.fire({
          icon: 'success',
          title: 'สำเร็จ',
        }).then(() => window.location = "/notice")
      }
    } catch (error) {
      console.log(error.response)
      alert(error.response.data)
      Swal.fire({
        icon: 'error',
        title: 'Oopss...',
        text: error.response.data
      })
    }
  }

  return (
    <div id="base" className="max-w-screen-2xl m-auto">
      <div className="flex justify-between my-8">
        <span className="text-6xl text-red-default font-extrabold">สรุปยอดเงิน</span>
        <Link to="/notice">
          <span className="text-6xl text-green-default font-extrabold cursor-pointer">แจ้งชำระเงิน<FontAwesomeIcon className='ml-4 pt-2' icon={faAngleRight} /></span>
        </Link>
      </div>
      <div className="bg-gray-200 rounded-lg p-4">
        <div className="flex text-xl font-bold justify-between text-blue-default">
          <span>ชื่อสินค้า </span>
          <span>{product.name}</span>
        </div>
        <div className="flex text-xl font-bold justify-between text-blue-default">
          <span>ภาษีมูลค่าเพิ่ม 7% </span>
          <span>{vat.toFixed(2)}</span>
        </div>
        <div className="flex justify-between font-bold text-xl text-blue-default">
          <span>ยอดรวม </span>
          <span>{product.price}</span>
        </div>
        <div className="flex justify-between mt-4 font-bold text-3xl text-red-fireBrick">
          <span>ยอดเงินที่ต้องชำระ</span>
          <span>{priceAll.toFixed(2)}</span>
        </div>
      </div>
      <div className="mt-16">
        <span className="text-6xl text-red-default font-extrabold ">ขอใบกำกับภาษี</span>
      </div>
      <form onSubmit={setDataInsert} className="w-full max-w-lg mt-6 mb-16">
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              เลขประจำตัวผู้เสียภาษี
            </label>
            <input onChange={(e) => { setTaxId(e.target.value) }} className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight" type="text" required></input>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              ชื่อ-นามสกุล
            </label>
            <input onChange={(e) => { setName(e.target.value) }} className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight" type="text" required></input>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              อีเมล
            </label>
            <input onChange={(e) => { setEmail(e.target.value) }} className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight" type="text" required></input>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              ที่อยู่
            </label>
            <textarea onChange={(e) => setAddressCustomer({ ...addressCustomer, address: e.target.value })} className="appearance-none block w-full bg-white text-gray-700 border border-gray-500 rounded py-3 px-4 mb-3 leading-tight" type="text" required></textarea>
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              เขต/อำเภอ
            </label>
            {/* <input onChange={(e) => { setSubDistrict(e.target.value) }} className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" required></input> */}
            <InputAddress
              style={styleAdrees}
              address="district"
              value={addressCustomer.district}
              onChange={onChange}
              onSelect={onSelect}
            />
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              แขวง/ตำบล
            </label>
            {/* <input onChange={(e) => { setDistrict(e.target.value) }} className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" required></input> */}
            <InputAddress
              address="subdistrict"
              style={styleAdrees}
              value={addressCustomer.subdistrict}
              onChange={onChange}
              onSelect={onSelect}
            />
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              จังหวัด
            </label>
            {/* <input onChange={(e) => { setProvince(e.target.value) }} className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" required></input> */}
            <InputAddress
              address="province"
              style={styleAdrees}
              value={addressCustomer.province}
              onChange={onChange}
              onSelect={onSelect}
            />
          </div>
        </div>
        <div className="flex flex-wrap -mx-3 mb-6">
          <div className="w-full px-3">
            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
              รหัสไปรษณีย์
            </label>
            {/* <input onChange={(e) => { setPostalCode(e.target.value) }} className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" required></input> */}
            <InputAddress
              address="zipcode"
              value={addressCustomer.zipcode}
              style={styleAdrees}
              onChange={onChange}
              onSelect={onSelect}
            />
          </div>
        </div>
        {/* <Link to="/notice">
          <button onClick={setDataInsert} className="w-full items-center bg-red-default text-xs sm:text-xl text-extrabold text-white  py-2 sm:py-3">แจ้งชำระเงิน</button>
        </Link> */}
        <div>
          <button type="submit" className="w-full items-center bg-red-default text-xs sm:text-xl text-extrabold text-white  py-2 sm:py-3 hover:bg-red-light">ขอใบกำกับภาษี</button>
        </div>
      </form>
    </div>
  )
}

export default EtaxDetail

import React, { useState, useEffect } from 'react'
import Header from '../../components/Header'
import '../../style/font.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faQuoteLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import CardProduct from '../../components/cardProduct'
import ApiGetProduct from '../../api/ApiGetProduct';
import { Link } from 'react-router-dom'

const Product = () => {

  // const dataCard = [{ name: "obey", status: "sale" }, { name: "obey2", status: "sale" }, { name: "obey3", status: "sale" }, { name: "obey4", status: "sale" }]
  const [dataCard, setDataCard] = useState([])

  useEffect(() => {
    getData();
    return () => {
    }
  }, []);

  const getData = async () => {
    try {
      const result = await ApiGetProduct.getProduct();
      if (result.status === 200) {
        const { data } = result.data;
        setDataCard(data);
      }
    } catch (error) {
      console.log(error)
    }
  }

  // const dataCard2 = [
  //   {
  //     "name": "trezor one",
  //     "description": "Hardware wallet",
  //     "price": 3400
  //   },
  //   {
  //     "name": "trezor model t",
  //     "description": "Hardware wallet",
  //     "price": 1000
  //   },
  //   {
  //     "name": "ledger nano x",
  //     "description": "Hardware wallet",
  //     "price": 2000
  //   },
  //   {
  //     "name": "ledger nano s",
  //     "description": "Hardware wallet",
  //     "price": 1500
  //   },

  // ]

  return (
    <div id="base">
      <div className="lg:mx-52 lg:my-20 flex justify-between">
        <div>
          <span className="text-6xl text-red-fireBrick font-extrabold mr-6">HARDWARE</span>
          <span className="text-6xl text-red-default font-extrabold">WALLET</span>
        </div>
        <Link to="/notice">
          <span className="text-6xl text-green-default font-extrabold cursor-pointer">แจ้งชำระเงิน<FontAwesomeIcon className='ml-4 pt-2' icon={faAngleRight} /></span>
        </Link>
      </div>
      <div className="grid grid-flow-row grid-cols-2 gap-x-7 gap-y-5 md:gap-y-16 md:gap-x-16 md:grid-cols-2 md:mx-6 md:my-10 lg:gap-y-16 lg:gap-x-16 lg:grid-cols-4 mx-6 my-10 lg:mx-52 lg:my-20">
        {dataCard.map((d, index) => {
          return (
            <div key={d.id}>
              <CardProduct name={d.name} description={d.description} point={d.price} id={d.id} image={d.image} />
            </div>
          );
        })}
      </div>
    </div>
  )
}

export default Product

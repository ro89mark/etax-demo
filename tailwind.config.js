const { green } = require('tailwindcss/colors')
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      white: colors.white,
      gray: colors.coolGray,
      pink: colors.fuchsia,
      blue: {
        light: "#E1EBFD",
        default: "#302ea7",
      },
      red: {
        default:"#EF8C27",
        fireBrick:"#B22222",
        light:"#E0BB50"
      },
      green: {
        default:"#32CD32",
      }
    },
    extend: {
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
